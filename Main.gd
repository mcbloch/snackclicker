extends Node2D

# Variables in the save game
signal score_change
var score = NAN
var roundedScore = NAN

signal autoclickLvl_change
var autoclickLvl = NAN

signal upgradeclickLvl_change
var upgradeclickLvl = NAN

var autoCost = 1000
var clickCost = 50

# Constants
const autoCostMultiplier = 1.1
const clickCostMultiplier = 1.1

signal newmultiplier_change
var newMultiplier = NAN

signal currentmultiplier_change
var currentMultiplier = NAN

var save_data = null

func _ready():
	connect("autoclickLvl_change", self, "_autoclick_lvl_changed")
	
	set_autoclick_lvl(save_data["autoclickLvl"])
	set_click_lvl(save_data["upgradeclickLvl"])
	set_score(save_data["score"])
	set_currentmultiplier(save_data["multiplier"])
	
	clickCost *= pow(clickCostMultiplier, upgradeclickLvl)
	autoCost *= pow(autoCostMultiplier, autoclickLvl)
	
	$drawer/upgradeClick.updateGUI()
	$drawer/upgradeAutoClick.updateGUI()
	$drawer/autoMultiplier.updateGUI()
	

func _process(delta):
	# Process the autoclicks
	inc_score(delta*autoclickLvl)
	
	# Quit on cancel pressed
	if Input.is_action_pressed("ui_cancel"):
		save()
		quit()
	
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		quit()

func quit():
	get_tree().quit() # default behavior

func save():
	Global.check_savegame(autoclickLvl, upgradeclickLvl, score, currentMultiplier)


# ################## # 
# Current multiplier #
# ################## #

func set_currentmultiplier(amount):
	currentMultiplier = amount
	emit_signal("currentmultiplier_change", amount)
	
# ##### # 
# Score #
# ##### #

func inc_score(amount):
	score += (currentMultiplier * amount)
	scoreUpdate()
	
func dec_score(amount):
	score -= amount
	scoreUpdate()
	
func set_score(amount):
	score = amount
	scoreUpdate()
	
func shortenValues(value):
	var scoreTitles = ["", "K", "M", "G", "T", "P", "E", "Z", "Y"]
	var shortenedScore = float(value)
	var i = 0
	while(shortenedScore >= 1000):
		shortenedScore = shortenedScore/1000
		i += 1
			
	#var steppedScore = shortenedScore
	var steppedScore = "%.0f"
	if(value >= 1000):
		if(shortenedScore < 10):
			steppedScore = "%.2f"
		elif(shortenedScore < 100):
			steppedScore = "%.1f"
		
	return {"label": scoreTitles[i],
			"value": steppedScore % shortenedScore}
	

func scoreUpdate():
	var newRoundedScore = int(round(score))
	
	if roundedScore != newRoundedScore:
		roundedScore = newRoundedScore
		
		# Update score label
		var shortened = shortenValues(roundedScore)
		emit_signal("score_change", shortened, roundedScore)


# ############# # 
# Upgrade Click #
# ############# #

func inc_click_lvl():
	set_click_lvl(upgradeclickLvl + 1)
	
func set_click_lvl(newLvl):
	upgradeclickLvl = newLvl
	emit_signal("upgradeclickLvl_change", upgradeclickLvl)
	

# ########## # 
# Auto Click #
# ########## #

func incAutoclickLvl():
	set_autoclick_lvl(autoclickLvl + 1)
	autoCost *= autoCostMultiplier

func set_autoclick_lvl(amount):
	autoclickLvl = amount
	emit_signal("autoclickLvl_change", autoclickLvl)

func _autoclick_lvl_changed(autoclickLvl):
	updateNewMultiplier()
	
func updateNewMultiplier():
	newMultiplier = (float(floor(autoclickLvl / 10)) * .1) + 1
	emit_signal("newmultiplier_change", newMultiplier)


# ##### # 
# Reset #
# ##### #

func applyReset():
	if newMultiplier > currentMultiplier:
		set_currentmultiplier(newMultiplier)
		
		set_score(0)
		set_autoclick_lvl(0)
		set_click_lvl(0)
		autoCost = 1000
		clickCost = 50
		scoreUpdate()
		updateNewMultiplier()