extends Sprite

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	scene.connect("upgradeclickLvl_change", self, "_on_clicklvl_changed")

func _on_clicklvl_changed(clicklvl):
	var snackLevel = floor(clicklvl / 10)
	set_frame(snackLevel)