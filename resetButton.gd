extends TextureButton

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	scene.connect("newmultiplier_change", self, "_on_multiplier_changed")
	scene.connect("currentmultiplier_change", self, "_on_multiplier_changed")

func _on_resetButton_pressed():
	get_node("../../ConfirmPopup").set_visible(true)

func _on_multiplier_changed(ignored):
	set_disabled(scene.newMultiplier <= scene.currentMultiplier)