extends Label

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	scene.connect("score_change", self, "on_score_change")
	
func on_score_change(shortened, roundedScore):
	set_text(shortened.value)