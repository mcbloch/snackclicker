extends TextureButton

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	scene.connect("score_change", self, "_on_score_changed")

func updateGUI():
	# Called on initialization and after every click update
	var shortened = scene.shortenValues(scene.autoCost)
	get_node("../autoPrice").set_text(shortened.value + shortened.label)
	get_node("progress").set_frame(int(scene.autoclickLvl) % 10)
	

func _on_upgradeAutoClick_pressed():
	if scene.roundedScore >= scene.autoCost:
		scene.dec_score(scene.autoCost)
		scene.incAutoclickLvl()
		
		updateGUI()
		
func _on_score_changed(shortened, roundedScore):
	set_disabled(roundedScore < scene.autoCost)