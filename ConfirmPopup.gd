extends Node

onready var scene = get_tree().get_root().get_node("Scene")

func _on_Confirm_pressed():
	scene.applyReset()
	get_node(".").set_visible(false)


func _on_Cancel_pressed():
	get_node(".").set_visible(false)