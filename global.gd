extends Node

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	# Initiate a savegame if one doesn't exist.
	if not savegame.file_exists(save_path):
		check_savegame(0, 0, 0, 1)
	
	savegame.open(save_path, File.READ)#OS.get_unique_ID())
	var json_result = JSON.parse(savegame.get_line())
	if json_result.error != OK:
		get_tree().quit()
		
	save_data = json_result.result
	savegame.close()
	
	scene.save_data = save_data
	
	# global_node.goto_scene("res://" + save_data["level"])

var save_data = {"firstrun":true, "autoclickLvl": NAN, "upgradeclickLvl": NAN, "score": NAN, "multiplier": NAN}
var savegame = File.new()
var save_path = "user://savegame.bin"

func check_savegame(autoLvl, upgradeLvl, score, multiplier):
	save_data["autoclickLvl"] = autoLvl
	save_data["upgradeclickLvl"] = upgradeLvl
	save_data["score"] = score
	save_data["multiplier"] = multiplier

	var err = savegame.open(save_path, File.WRITE)#OS.get_unique_ID())
	savegame.store_line(JSON.print(save_data))
	savegame.close()