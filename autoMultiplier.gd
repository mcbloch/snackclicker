extends Label

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	scene.connect("newmultiplier_change", self, "_on_newmultiplier_changed")
	
func _on_newmultiplier_changed(newMultiplier):
	updateGUI()

func updateGUI():
	get_node("../autoMultiplier").set_text(("%.1f" % (scene.newMultiplier)) + "x")