extends Area2D

onready var scene = get_tree().get_root().get_node("Scene")

func _on_Snack_input_event(viewport, event, shape_idx):
	if event.is_pressed() and Input.is_action_pressed("primary_select"):
		scene.inc_score(1 * (scene.upgradeclickLvl + 1))
		get_node("../Snack/AnimationPlayer").play("click")