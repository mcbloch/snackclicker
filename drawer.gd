extends Sprite

var last_animation = "closeDrawer"

func _on_TextureButton_pressed():
	if last_animation == "closeDrawer":
		last_animation = "openDrawer"
	else:
		last_animation = "closeDrawer"
		
	$AnimationPlayer.play(last_animation)
