extends TextureButton

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	scene.connect("score_change", self, "_on_score_changed")
	scene.connect("upgradeclickLvl_change", self, "_on_clicklvl_changed")

func updateGUI():
	var shortened = scene.shortenValues(scene.clickCost)
	get_node("../clickPrice").set_text(shortened.value + shortened.label)

func _on_upgradeClick_pressed():
	if scene.roundedScore >= scene.clickCost:
		scene.dec_score(scene.clickCost)
		scene.inc_click_lvl()
		scene.clickCost *= scene.clickCostMultiplier
		
		updateGUI()
		
func _on_score_changed(shortened, roundedScore):
	set_disabled(roundedScore < scene.clickCost)
	
func _on_clicklvl_changed(clicklvl):
	$progress.set_frame(int(clicklvl) % 10)
	
	var snackLevel = floor(scene.upgradeclickLvl / 10)
	get_tree().get_root().get_node("Scene/Snack/Sprite").set_frame(snackLevel)