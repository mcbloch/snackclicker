extends Label

onready var scene = get_tree().get_root().get_node("Scene")

func _ready():
	updateGUI()
	scene.connect("currentmultiplier_change", self, "_on_currentmultiplier_changed")
	
func _on_currentmultiplier_changed(currentMultiplier):
	updateGUI()
	
func updateGUI():
	set_text(str(scene.currentMultiplier) + " x")